package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

    static final String APIBaseUrl = "/apitechu/v1";

    @GetMapping(APIBaseUrl + "/products")
    public ArrayList<ProductModel> getProducts(){
        System.out.println("getProducts");

        return ApitechuApplication.productModels;
    }

    @GetMapping(APIBaseUrl + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("id es: " + id);

        ProductModel result = new ProductModel();

        for (ProductModel product : ApitechuApplication.productModels) {
            if(product.getId().equals(id)){
                result = product;
            }
        }

        return result;
    }

    @PostMapping(APIBaseUrl + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct) {
        System.out.println("createProduct");
        System.out.println("La id del nuevo producto es: " + newProduct.getId());
        System.out.println("La Descripción del nuevo producto es: " + newProduct.getDesc());
        System.out.println("El precio del nuevo producto es: " + newProduct.getPrice());

        ApitechuApplication.productModels.add(newProduct);

        return newProduct;
    }

    @PutMapping(APIBaseUrl + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product,
                                      @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("La id del producto a actualizar en parametro URL es: " + id);
        System.out.println("La id del producto a actualizar es: " + product.getId());
        System.out.println("La descripcion del producto a actualizar es: " + product.getDesc());
        System.out.println("el precio del producto a actualizar es: " + product.getPrice());

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if(productInList.getId().equals(id)){
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }

        return product;
    }

    @DeleteMapping(APIBaseUrl + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id){
        System.out.println("deleteProduct");
        System.out.println("El id del Producto a Borrar es: " + id);

        ProductModel result = new ProductModel();

        boolean foundCompany = false;

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if(productInList.getId().equals(id)){
                System.out.println("Producto Encontrado");
                foundCompany = true;
                result = productInList;
            }
        }

        if(foundCompany == true) {
            System.out.println("Borrando Producto");
            ApitechuApplication.productModels.remove(result);
        }

        return result;
    }

//    @PatchMapping(APIBaseUrl + "/products/{id}")
//    public ProductModel patchProduct(
//            @PathVariable String id,
//            @RequestParam(name="desc",defaultValue = "") String desc,
//            @RequestParam(name="price",defaultValue = "0") float price
//    ){
//        System.out.println("patchProduct");
//        System.out.println("El id del Producto a Actualizar es: " + id);
//        System.out.println("La desc del Producto a Actualizar es: " + desc);
//        System.out.println("El precio del Producto a Actualizar es: " + price);
//
//        ProductModel result = new ProductModel();
//
//        for (ProductModel productInList : ApitechuApplication.productModels) {
//            if(productInList.getId().equals(id)){
//                System.out.println("Producto Encontrado");
//                if (desc != null && !desc.equals("")) {
//                    System.out.println("Actualizar descipción");
//                    productInList.setDesc(desc);
//                }
//                if (price > 0) {
//                    System.out.println("Actualizar precio");
//                    productInList.setPrice(price);
//                }
//                result = productInList;
//            }
//        }
//
//        return result;
//
//    }

    @PatchMapping(APIBaseUrl + "/products/{id}")
    public ProductModel patchProduct(@PathVariable String id, @RequestBody ProductModel productData){
        System.out.println("patchProduct");
        System.out.println("El id del Producto a Actualizar es: " + id);
        System.out.println("La desc del Producto a Actualizar es: " + productData.getDesc());
        System.out.println("El precio del Producto a Actualizar es: " + productData.getPrice());

        ProductModel result = new ProductModel();

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if(productInList.getId().equals(id)){
                System.out.println("Producto Encontrado");
                result = productInList;
//                productData.getDesc() != null ? productInList.setDesc(productData.getDesc());
//                productData.getPrice() > 0 ? productInList.setPrice(productData.getPrice());
                if (productData.getDesc() != null) {
                    System.out.println("Actualizando la descipción del Producto");
                    productInList.setDesc(productData.getDesc());
                }
                if (productData.getPrice() > 0) {
                    System.out.println("Actualizando el precio del Producto");
                    productInList.setPrice(productData.getPrice());
                }
            }
        }

        return result;

    }

}
